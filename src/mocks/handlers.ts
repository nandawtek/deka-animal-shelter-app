import { rest } from 'msw'
import { HOST_URL } from '../default.config'

export const handlers = [
  rest.post(`${HOST_URL}/v1/pets`, (req, res, ctx) => {
    return res(
      ctx.json({
        status: 'ok',
        success: [],
        failed: [],
      })
    )
  }),
]
