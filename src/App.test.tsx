import 'reflect-metadata';
import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('Renders the main page', () => {
  render(<App />);
  const linkElement = screen.getByText(/Animal shelter/i);
  expect(linkElement).toBeInTheDocument();
});
