import React, { ChangeEvent, useState } from 'react'
import { Form, Button, Spinner, Table, Image, Alert } from 'react-bootstrap'
import Container from 'typedi'
import If from '../components/If'
import Page from '../components/Page'
import Api, { UploadResult } from '../services/Api'
import { FaExclamationTriangle, FaAngellist } from 'react-icons/fa'

export interface AnimalsInputPageProps {}

const api = Container.get(Api)

export type InputUploadState = 'ONGOING' | UploadResult | null

export function isResultType(what: InputUploadState): what is UploadResult {
  return what != null && typeof what !== 'string'
}

export function mapAndZipStatus(status: InputUploadState) {
  if (!isResultType(status)) {
    throw new Error('No plz')
  }
  return [
    ...status.failed.map((s) => ({ ...s, status: 'FAILED' as const })),
    ...status.succeed.map((s) => ({ ...s, status: 'SUCCEED' as const })),
  ]
}

export default function AnimalsInputPage(props: AnimalsInputPageProps) {
  const [csv, setCsv] = useState<File | null>(null)
  const [status, setStatus] = useState<InputUploadState>(null)
  async function sendDocument() {
    if (csv == null) {
      return alert('¡Debes especificar un archivo!')
    }
    setStatus('ONGOING')
    const { response, data } = await api.sendCsv(csv)
    if (data.status !== 'ok') {
      setStatus(null)
      alert(`El servidor ha respondido: ${(data as any).message}`)
    } else {
      setStatus(data)
    }
  }
  function documentChanged(e: ChangeEvent<HTMLInputElement>) {
    const file = e.target?.files?.[0]
    if (file == null) {
      return alert('Ha ocurrido un error inesperado')
    }
    setCsv(file)
  }
  return (
    <Page title="Animal shelter">
      <p>
        Este formulario sirve para que tu subas animales a nuestras redes
        sociales. Debes subir un CSV con estas columnas: imagen, contacto,
        título y descripción.
      </p>
      <Form>
        <Form.Group>
          <Form.Label>Archivo CSV</Form.Label>
          <Form.Control
            data-testid="csv-input"
            onChange={documentChanged}
            type="file"
          />
        </Form.Group>
        <Form.Group>
          <Button
            disabled={csv == null}
            data-testid="send-csv"
            onClick={sendDocument}
          >
            Enviar
          </Button>
        </Form.Group>
      </Form>

      <br />

      <div>
        <p>
          Puedes utilizar esta plantilla para subir tus mascotas a la base de
          datos:
        </p>
        <Button href="/resources/petsCSV.csv">Descargar ejemplo</Button>
      </div>

      <If
        what={status === 'ONGOING'}
        then={() => (
          <Spinner animation="border" role="status">
            <span className="visually-hidden">
              Contactando con el servidor...
            </span>
          </Spinner>
        )}
        else={() => (
          <If
            what={isResultType(status)}
            then={() => (
              <>
                <div className="spacer"> </div>
                <h4>Resultado</h4>
                <Table striped hover>
                  <thead>
                    <tr>
                      <th>Imagen</th>
                      <th>Título</th>
                      <th>Descripción</th>
                      <th>Contacto</th>
                      <th>Estado</th>
                      <th>Predicción</th>
                    </tr>
                  </thead>
                  <tbody>
                    {mapAndZipStatus(status).map(
                      ({
                        image,
                        title,
                        description,
                        contact,
                        probability,
                        status,
                      }) => (
                        <tr>
                          <td>
                            <Image
                              style={{ maxWidth: '5rem' }}
                              rounded
                              src={image}
                            />
                          </td>
                          <td>{title}</td>
                          <td>{description}</td>
                          <td>{contact}</td>
                          <If
                            what={status === 'FAILED'}
                            then={() => (
                              <td className="danger-cell">
                                <FaExclamationTriangle /> FALLIDO
                              </td>
                            )}
                            else={() => (
                              <td className="success-cell">
                                <FaAngellist /> Correcto
                              </td>
                            )}
                          />
                          <td>{Math.round(probability * 100 * 10) / 10}%</td>
                        </tr>
                      )
                    )}
                  </tbody>
                </Table>
              </>
            )}
          />
        )}
      />
    </Page>
  )
}
