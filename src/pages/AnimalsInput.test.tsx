import 'reflect-metadata'
import React from 'react'
import { render, screen, fireEvent, waitFor } from '@testing-library/react'
import AnimalsInputPage from './AnimalsInputPage'
import { readFileSync } from 'fs'
import { server } from '../mocks/server'

beforeAll(() => {
  server.listen()
})

afterEach(() => {
  server.resetHandlers()
})

afterAll(() => {
  server.close()
})

test('title of the animals input page', () => {
  render(<AnimalsInputPage />)
  const linkElement = screen.getByText(/Animal shelter/i)
  expect(linkElement).toBeInTheDocument()
})

test('description of the animals input page', () => {
  render(<AnimalsInputPage />)
  const linkElement = screen.getByText(
    /Este formulario sirve para que tu subas animales a nuestras redes sociales. Debes subir un CSV con estas columnas/i
  )
  expect(linkElement).toBeInTheDocument()
})

test('document can be sent to the API', async () => {
  const blob = readFileSync(`${process.cwd()}/src/mocks/petsCSV.csv`)
  const file = new File([blob], 'petsCSV.csv')
  render(<AnimalsInputPage />)
  const input = screen.getByTestId('csv-input')
  await waitFor(() =>
    fireEvent.change(input, {
      target: { files: [file] },
    })
  )
  const submit = screen.getByTestId('send-csv')
  fireEvent.click(submit)
  await waitFor(() => screen.getAllByText(/Contactando con el servidor.../))
  const modal = screen.getByText(/Contactando con el servidor.../i)
  expect(modal).toBeInTheDocument()
})

test('animals sample CSV file', () => {
  render(<AnimalsInputPage />)
  const link = screen.getByText(/Descargar ejemplo/i)
  expect(link).toBeInTheDocument()
  expect(link).toHaveAttribute('href')
})
