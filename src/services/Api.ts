import { Service } from 'typedi'
import { HOST_URL } from '../default.config'

export type ApiResponse<T> = {
  response: Response
  data: T
}

export interface Pet {
  image: string
  description: string
  title: string
  contact: string
  probability: number
}

export interface UploadResult {
  status: 'ok'
  succeed: Pet[]
  failed: Pet[]
}

@Service()
export default class Api {
  async request<T>(
    input: RequestInfo,
    init?: RequestInit
  ): Promise<ApiResponse<T>> {
    const response = await fetch(`${HOST_URL}${input}`, init)
    const data = await response.json()
    return { response, data }
  }

  async sendCsv(file: File) {
    const body = new FormData()
    body.append('files', file)
    return await this.request<UploadResult>('/v1/pets', {
      method: 'POST',
      body,
    })
  }
}
