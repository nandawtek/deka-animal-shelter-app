export interface IfProps<A, R, E> {
  what?: A
  then?: (what: A) => R
  else?: (what?: A) => E
}

export default function If<A, R, E>({
  what,
  then,
  else: e,
}: IfProps<A, R, E>): any {
  if (what) {
    return then?.(what) ?? null
  } else {
    return e?.(what) ?? null
  }
}
