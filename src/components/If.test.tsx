import React from 'react'
import { render, screen } from '@testing-library/react'
import Page from './Page'
import If from './If'

test('it should contain the truthy result', () => {
  render(
    <If
      what={true}
      then={() => <div>truthy text</div>}
      else={() => <div>falsy text</div>}
    />
  )
  const element = screen.getByText(/truthy text/i)
  expect(element).toBeInTheDocument()
})

test('it should contain the falsy result', () => {
  render(
    <If
      what={false}
      then={() => <div>truthy text</div>}
      else={() => <div>falsy text</div>}
    />
  )
  const element = screen.getByText(/falsy text/i)
  expect(element).toBeInTheDocument()
})
