import React from 'react';
import { render, screen } from '@testing-library/react';
import Page from './Page';

test('renders a page with a simple title', () => {
  render(
    <Page title="Animal shelter"></Page>
  );
  const linkElement = screen.getByText(/Animal shelter/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders a page with a simple description', () => {
  render(
    <Page title="">Body content</Page>
  );
  const linkElement = screen.getByText(/Body content/i);
  expect(linkElement).toBeInTheDocument();
});
