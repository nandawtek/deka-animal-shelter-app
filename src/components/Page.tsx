import React from 'react';
import './Page.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row } from 'react-bootstrap';

export interface PageProps {
  children?: any;
  title: string;
}

export default function Page(props: PageProps) {
  return (
    <div>
      <Container>
        <Row>
          <h1>{props.title}</h1>
          {props.children}
        </Row>
      </Container>
    </div>
  );
}
