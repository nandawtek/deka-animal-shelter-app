import React from 'react'
import AnimalsInputPage from './pages/AnimalsInputPage'

export default function App() {
  return <AnimalsInputPage />
}
